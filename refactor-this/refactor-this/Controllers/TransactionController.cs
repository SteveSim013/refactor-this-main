﻿using refactor_this.Database;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace refactor_this.Controllers
{
    //Added simple Authorization - TODO: needs an Identity provider setup still. Can define roles here as well
    [Authorize]
    public class TransactionController : ApiController
    {
        [HttpGet, Route("api/Accounts/{id}/Transactions")]
        public IHttpActionResult GetTransactions(Guid id)
        {

            try
            {

                using (var context = new Database.TransactionsAccounts())
                {
                    var transactions = context.Transactions.Where(o => o.Id == id).FirstOrDefault();
                    return Ok(transactions);
                }
            }
            catch (Exception exc)
            {
                //TODO: Setup loggging for transaction request failure
                return null;
            }
           
        }

        [HttpPost, Route("api/Accounts/{id}/Transactions")]
        public IHttpActionResult AddTransaction(Transaction transaction)
        {

            try
            {

                using (var context = new Database.TransactionsAccounts())
                {
                    context.Transactions.Add(transaction);
                    context.SaveChanges();
                    //TODO: Modify how we update/retrieve the account amounts to be dynamically calculated.
                    return Ok();
                }
            }
            catch (Exception exc)
            {
                //TODO: Setup loggging for transaction failure
                return null;
            }

        }
    }
}