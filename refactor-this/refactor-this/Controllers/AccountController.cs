﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using refactor_this.Database;

namespace refactor_this.Controllers
{
    //Added simple Authorization - TODO: needs an Identity provider setup still. Can define roles here as well
    [Authorize]
    public class AccountController : ApiController
    {
        [HttpGet, Route("api/Accounts/{id}")]
        public IHttpActionResult GetById(Guid id)
        {
            using (var connection = Helpers.NewConnection())
            {
                return Ok(Get(id));
            }
        }

        [HttpGet, Route("api/Accounts")]
        public IHttpActionResult Get()
        {
            using (var context = new Database.TransactionsAccounts())
            {
                return Ok(context.Accounts.ToList());
            }
        }

        private Account Get(Guid id)
        {
            try
            {

                using (var context = new Database.TransactionsAccounts())
                {
                    var account = context.Accounts.Where(o => o.Id == id).FirstOrDefault();
                    if (account != null)
                        return account;
                    else
                        throw new Exception("No account by this id found");
                }
            }
            catch(Exception exc)
            {
                //TODO: Setup loggging for account request failure
                //Return type is expecting an account, change ways so it can send back an error if it cannot be found etc.
                return null;
            }
        }

        [HttpPost, Route("api/Accounts")]
        public IHttpActionResult Add(Account account)
        {
            try
            {

                using (var context = new Database.TransactionsAccounts())
                {
                    context.Accounts.Add(account);
                    context.SaveChanges();
                    return Ok();
                    
                }
            }
            catch (Exception exc)
            {
                //TODO: Setup loggging for account add failure and nice message back to user
                return InternalServerError(exc);
            }
        }

        [HttpPut, Route("api/Accounts/{id}")]
        public IHttpActionResult Update(Account account)
        {
            try
            {

                using (var context = new Database.TransactionsAccounts())
                {
                    context.Entry(account).State = System.Data.EntityState.Modified;
                    context.SaveChanges();
                    return Ok();

                }
            }
            catch (Exception exc)
            {
                //TODO: Setup loggging for account update failure and nice message back to user
                return InternalServerError(exc);
            }
        }

        [HttpDelete, Route("api/Accounts/{id}")]
        public IHttpActionResult Delete(Guid id)
        {
            try
            {

                using (var context = new Database.TransactionsAccounts())
                {
                    var account = context.Accounts.Where(o => o.Id == id).FirstOrDefault();
                    if (account != null)
                    {
                        context.Accounts.Remove(account);
                        context.SaveChanges();
                        return Ok();
                    }
                    else
                    {
                        throw new Exception("No account found");
                    }

                }
            }
            catch (Exception exc)
            {
                //TODO: Setup loggging for account delete failure and nice message back to user
                return InternalServerError(exc);
            }
        }
    }
}