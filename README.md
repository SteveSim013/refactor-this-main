# Refactor/Review of the refactor-this project.


	Downloaded 26/10/2020 around 1.45pm

	Spend roughly 1 hour - plus some time spent on admin, getting Atlassian account revived and working firewall/Group Policy issues with my PC I used to do this.


# Actions:

	Had to run Update-Package Microsoft.CodeDom.Providers.DotNetCompilerPlatform -r to get the roslyn compiler to work in the Package Manager console.
	
	I have converted to EntityFramework so the domain/model can be maintained at the Database level and then the 
	coding can be tidier instead of hard-coded SQL/Old fashioned execute-command style.

	(We can use a Model that uses the EF generated model with partial clases/meta classes for specific business-model requirements or other transformations.)
	As this is very simple - we can just use the Domain model directly for simplicity. Although not recommended for larger scale.

	Added  basic try catch around the transactions: TODO : To add logging and nicer handling of errors.
	Removed excess inputs for the Account Update in the AccountController. (Only needs the account model). Did the same for the TransactionController.
	
# Future Development / Changes

	Noticed the mix methods of return methods in the Account Controller - IHttpActionResult and a direct Account object being returned 
		- I would probably change all of them to use a common return style.  (I did not complete this in time)

	Account model/database entry has a hardcoded amount of the curent amount - could changed to a Calculated Column in SQL, or delegate it 
	to a service that updates a common data model for the Account amounts.  eg (Depending on scope) - a separate table for the current account information
	could be created to work out on the fly the live amount of funds in it, along with keeping historical information in a history table/datawarehouse. 
	(Again, depending on scope, you can quickly over-engineer a small project with things like Transaciton history/data warehouse when simple date-stamped data could
	be used to work it out.)

 -Database 
 
	I would create a SQL database and migrate the MDF accross but for scale it may not be nessescary if it is a small scope.

 -Naming Conventions
 
	Come up with a name for the project that reflects what the product is.
	At present I have named it AccountsTransactions (For the sake of EntityFrameworks model/namespace requirements)
 
 -Nice to haves
 
	Add more useful data to suit accounts and transactions, ie, what kind of transaction, information about it etc.


# Access Improvements

	Authorization needs to be added to the Controllers/Routes - at the moment it is Allow all.
	I have added a simple [Authorize] Attribute to the existing controllers but to connect it up to a identity provider with an access model & roles will take some time.


# Design Improvements:


	Depending on scale (This project is tiny, what's the scope?) it may possible to use dynamic routing 
	to replace hard-coded Actions/Routes in the controller, and dynamically load them from a config/database table.
	That way you would only need one Controller (eg DataController) that does all the handling of loading/saving information, the access layer and routing.


# Database Improvements

	Using EntityFramework for the project moving the refactor-this.mdf to its own Database on SQL Server.
	We could either use CodeFirst or Database first (It's the developers choice really, but Database first is 
	more traditional). mdf is fine for a small project like this, however, Azure hosting has a problem with .mdf files that forces you to use Azure SQL. 
	(I even tried a SQL Compact Edition file!) Other forms of cloud hosting would probably be fine though.




